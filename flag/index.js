function hoist() {
    var flag = document.getElementById("flag")
    console.log(typeof flag)
    var initial = 80;
    var interval = setInterval(function () {
        initial = initial - 1
        console.log(initial)
        if (initial > 0) {
            flag.style.top = (initial) + 'vh'
        }
        if (initial === 0) {
            clearInterval(interval)
        }
        console.log(flag.style.top)
    }, 50)
}
function unHoist() {
    var flag = document.getElementById("flag")
    console.log(typeof flag)
    var initial = 0;
    var interval = setInterval(function () {
        initial = initial + 1
        console.log(initial)
        if (initial < 82) {
            flag.style.top = (initial) + 'vh'
        }
        if (initial === 82) {
            clearInterval(interval)
        }
        console.log(flag.style.top)
    }, 50)
}